import sys,os
from gy25_Parser import gy25_Parser
from SerialExtractor import SerialExtractor
from matlab_export import export_to_matlab

class gy25_API:
    '''
    gy25_API can be used for pulling data from the gy25-tilt module via UART connection
    
    (init) Args:
        rawDataFilename (str): Filepath to file to which the raw data should be exported
        outFilepath     (str): Filepath to folder into which the parsed data should be exported
    '''

    def __init__(self, rawDataFilename, outFilepath):

        self.rawDataFilename = rawDataFilename
        self.outFilepath = outFilepath

    def serial_read_and_write(self, dev, baud, mode, timeToRun, bytesToRead = 31, timeBetweenReads = 0):
        '''
        Pulls raw data from serial and afterwards parses the data to human readable format

        Args:
            dev         (str): Name of the serial device
            baud        (int): Baudrate for serial communication
            mode        (str): Specifies gy25 communication mode (Query or Auto)
            timeToRun   (int): Specifies time in seconds for how long the serial communication should last 

            bytesToRead         (int, optional): Specifies how many bytes to read in single read (Default = 31)
            timeBetweenReads    (int, optional): Specifies waiting time between serial read (Default = 0)
        '''
        SE = SerialExtractor(dev, baud, mode, self.rawDataFilename, bytesToRead, timeBetweenReads)
        SE.read_and_write(timeToRun)

    def parse_and_write(self):
        '''
        Parses and exports raw data extracted from gy25 to a file
        '''

        Parser = gy25_Parser(self.rawDataFilename, self.outFilepath + 'parsed_data')
        Parser.read_raw_parse_write()

    def parsed_file_to_matlab(self):
        '''
        Converts and exports parsed gy25 information to matlab format
        '''
        export_to_matlab(self.outFilepath + 'parsed_data' , self.outFilepath + 'result.mat')

import numpy as np
import scipy.io as sio
from datetime import datetime

def file_to_matrix(dataFilename):
    matrix = list()

    with open(dataFilename, 'r') as f:

        line = f.readline()

        while line is not '':
            try:
                matrix.append([float(i) for i in line.split(' ')[:-1]])
            except:
                pass
            line = f.readline()

    return np.array(matrix)

def export_to_matlab(dataFilename, outputFilename):
    sio.savemat(outputFilename, {'measurements': file_to_matrix(dataFilename)})
    print('Output saved to {}'.format(outputFilename))

